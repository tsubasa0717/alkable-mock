# 導入方法
### Gulp インストール
```
npm install -D gulp

```

### Gulp スタート
```
gulp
```

### アクセス
http://localhost:8000
Cannot GET / と表示された場合、終了後再スタート

### Gulp 終了
control + C 