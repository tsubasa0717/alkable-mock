var gulp = require('gulp'),

    // 出力先ディレクト
    dist = './dist',
    //　ソースディレクトリ
    src = './src';

module.exports = {
    src: src,
    dist: dist,
    // sass
    sass: {
        src: src + '/assets/scss/**/*.scss',
        dist: assetsdist + '/assets/css',
        options: {
            sourceMap: !release,
            precision: 5,
            outputStyle: release ? 'compressed' : 'expanded',
        },
        autoprefixer: {
            browsers: ['last 5 versions', 'ie 9']
        },
        sourcemaps: {
            includeContent: !release
        }
    },
    // ejs
    ejs: {
        src: [
            src + '/**/!(_)*.ejs'
        ],
        dist: dist,
        debug: !release,
        minify: false,
        ext : args.php_container ? 'php' : 'html',
        options: {
            root: src
        }
    },
    // watch
    watch: {
        bower: './bower.json',
        js: relativeSrcPath + '/assets/js/**/*.js',
        webpack: [
            relativeSrcPath + '/assets/ts/**/*.ts'
        ],
        ts: relativeSrcPath + '/assets/ts/HeaderFooter.ts',
        riot: relativeSrcPath + '/assets/riot/**/*.tag',
        scss: relativeSrcPath + '/assets/scss/**/*.scss',
        img: [relativeSrcPath + '/assets/img/**/*'],
        ejs: [relativeSrcPath + '/**/*.ejs', './gulp/ejsconfig.json'],
        html: [relativeSrcPath + '/**/*.html'],
        other: [
            '!./src/**/*.map',
            './src/**/.htaccess',
            './src/**/*.mp4',
            './src/**/*.swf',
            './src/**/*.php',
            './src/**/*.json',
            './src/root/**/*.js'
        ]
    }
};



// HELP TOOLS.
var black = '\u001b[30m';
var red = '\u001b[31m';
var green = '\u001b[32m';
var yellow = '\u001b[33m';
var blue = '\u001b[34m';
var magenta = '\u001b[35m';
var cyan = '\u001b[36m';
var white = '\u001b[37m';

if (args._[0] === 'ls' || args._[0] === 'list') {
    gulp.task('ls', taskListing);
    console.log(cyan + 'Now proccesing...');
}

if (args._[0] === undefined) {
    console.log('\n\n\n');
    console.log(cyan + '----------------------------------------');
    console.log(cyan + '-   Starting watch & server process!   -');
    console.log(cyan + '-   Have a nice development  ٩( ᐛ )و   -');
    console.log(cyan + '----------------------------------------');
    console.log('\n');
}

if (args.h || args.help) {
    console.log('\n');
    console.log(cyan + 'gulp -d=<directory path>    It specifies the root directory of the project.');
    console.log(cyan + 'gulp -s=<directory path>    Use this when the project route and the server route is different.');
    console.log(cyan + 'gulp -r || --release        Use to make the build release.');
    console.log(cyan + 'gulp -h || --help           This');
    console.log(cyan + 'gulp ls || list             Show task lists.');
    console.log('\n');
    console.log(white + 'Have a nice day ٩( ᐛ )و');
    process.exit();
}

if (!args.d) {
    console.log(white + '(・ε・)ﾃｯﾃﾚｰ ');
    console.log(red + 'Be use this argument. -> -d=<directory path>');
    console.log(red + 'If you want see the list of task -> gulp ls');
    process.exit();
}

if (!fs.existsSync(src) || !fs.statSync(src).isDirectory()) {
    console.log(white + '╭( ･ㅂ･)و ' + red + 'Project directory does not exist. : ' + src);
    process.exit();
}

if (!release && args._[0] === 'build' && !args.force) {
    console.log(white + '(　˙-˙　)? ' + red + 'Are you really order for debugging build?');
    console.log('\n');
    console.log(red + 'If add \'-release\', build for production.');
    console.log(red + 'Or add \'--force\', build for debugging.');
    process.exit();
}