const path = require('path');
const gulp = require('gulp');
const watch = require('gulp-watch');

var relativeSrcPath = path.relative('.', './src');

var config = {
        assets: relativeSrcPath + '/assets/**',
        sass: relativeSrcPath + '/**/sass/*.sass',
        ejs: [relativeSrcPath + '/**/*.ejs', './gulp/ejsconfig.json']
};

gulp.task('watch', function() {

    // assets
    watch(config.assets, function() {
        gulp.start(['assets']);
    });

    // sass
    watch(config.sass, function() {
        gulp.start(['sass']);
    });

    // ejs
    watch(config.ejs, function() {
        gulp.start(['ejs']);
    });
});