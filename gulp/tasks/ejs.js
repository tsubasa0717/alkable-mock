const gulp = require("gulp");
const ejs = require("gulp-ejs");
const rename = require("gulp-rename");

gulp.task("ejs", function() {

    var settingsFilename = '../ejsconfig.json';
    var settings = require(settingsFilename);

    return gulp.src(['src/**/*.ejs','!' + "src/**/_*.ejs"])
        .pipe(ejs(settings))
        .pipe(rename(function (path) {
            path.extname = ".html";
            console.log('ejs → html changed');
        }))
        .pipe(gulp.dest('dist/'));

});
