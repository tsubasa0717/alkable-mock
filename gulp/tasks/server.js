const gulp = require('gulp');
const webserver = require('gulp-webserver');

gulp.task('server', function () {
    gulp.src('./dist')
        .pipe(webserver({
            host: 'localhost',
            port: 8000,
            livereload: true
        }));
});