const gulp = require('gulp');

gulp.task( 'assets', function() {
    return gulp.src(
        [ 'src/assets/**' ],
        { base: 'src' }
    )
        .pipe( gulp.dest( 'dist' ) );
});