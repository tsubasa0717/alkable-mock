const path = require('path');
const gulp = require('gulp');

var relativeSrcPath = path.relative('.', './src');

var config = {
        sass: relativeSrcPath + '/**/sass/*.sass',
        ejs: [relativeSrcPath + '/**/ejs/*.ejs', './gulp/ejsconfig.json']
};

// sass
gulp.task('init', function () {

    gulp.start(['assets']);

    gulp.src(config.sass, function() {
        gulp.start(['sass']);
    });

    gulp.src(config.ejs, function() {
        gulp.start(['ejs']);
    });

});