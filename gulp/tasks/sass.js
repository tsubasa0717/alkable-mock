const gulp = require('gulp');
const sass = require('gulp-sass');
const replace = require("gulp-replace");
const rename = require("gulp-rename");

gulp.task("sass", function() {
    gulp.src('src/**/sass/*.sass',{ base: 'src/**/sass' })
        .pipe(sass({
            outputStyle: 'expanded'
        }))
        .pipe(rename(function (path) {
            path.dirname = path.dirname.replace( 'sass', 'css' );
            path.dirname = path.dirname.replace( '../../', '' );
            dir = path.dirname.split("/");
            path.dirname = path.dirname.replace( dir[0], 'assets' );
            path.basename = path.basename.replace( 'style', dir[0] );
        }))
        .pipe(gulp.dest('./dist'));
    console.log('sass → css changed');
});